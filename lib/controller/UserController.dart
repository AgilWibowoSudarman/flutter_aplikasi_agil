import 'package:appsi/data/user/imageuser.dart';
import 'package:flutter/material.dart';

class UserController extends ChangeNotifier {
  final User _user = User(
      userName: 'Agil',
      imageUrl: 'https://source.unsplash.com/random/120x120?portrait',
      job: 'Photographer',);
      bool _isLogin = false;

  User get user => _user;
  bool get isLogin => _isLogin;



  void login() {
    _isLogin = true;
    notifyListeners();
  }
   void logout() {
    _isLogin = false;
    notifyListeners();
  }
}
