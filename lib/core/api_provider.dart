import 'dart:io';

import 'package:dio/dio.dart';


const baseUrl = 'https://api.unsplash.com/';
const apiKey = 'qXovN9tFKEiw234ku_u_9_p8B9OZp69lUUQA3WM45jA';

class ApiProvider {
  Dio get dio => _dio();

  Dio _dio() {
    var dio = Dio(options());
    return dio;
  }

  BaseOptions options() {
    var options = BaseOptions(
      baseUrl: baseUrl,
      contentType: 'application/json',
      responseType: ResponseType.plain,
      headers: {
        HttpHeaders.authorizationHeader: 'Client-ID $apiKey',
      },
    );
    return options;
  }
}