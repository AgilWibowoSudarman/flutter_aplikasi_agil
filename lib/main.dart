import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:appsi/controller/photo_controller.dart';

import 'ui/app_widget.dart';

void main() {
  runApp(const AppWidget());
}
