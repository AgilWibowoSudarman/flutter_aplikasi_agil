class User {
  final String userName;
  final String imageUrl;
  final String job;

  User({
    required this.userName,
    required this.imageUrl,
    required this.job,
  });
}
